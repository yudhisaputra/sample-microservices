package rebelworks.co.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import rebelworks.co.model.CurrencyConversion;

//@FeignClient(name="Sample-Microservices-Forex", url="localhost:8000")
/*Penambahan Load Balancing*/
@FeignClient(name="forex-service")
@RibbonClient(name="forex-service")
public interface CurrencyConversionService {

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversion retrieveExchangeValue(@PathVariable("from") String from, @PathVariable("to") String to);
}