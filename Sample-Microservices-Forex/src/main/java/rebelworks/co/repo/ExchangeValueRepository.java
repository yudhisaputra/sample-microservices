package rebelworks.co.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import rebelworks.co.model.ExchangeValue;

/**
*
* @author yudhi.saputra@rebelworks.co
*/
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long>{
	ExchangeValue findByFromAndTo(String from, String to);
}